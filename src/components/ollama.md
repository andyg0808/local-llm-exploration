## Ollama
I booted [Ollama](https://ollama.ai/) with
```sh
docker run -d -v ollama:/root/.ollama -p 11434:11434 --name ollama --network anythingllm --network-alias ollama ollama/ollama
```

New [models](https://ollama.ai/library) can be loaded with
```sh
docker exec -it ollama ollama run $modelname
```
and then Ctrl-d to quit once the model downloads. It'll then be available in the AnythingLLM menus.