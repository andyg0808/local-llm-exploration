## AnythingLLM
I found [AnythingLLM](https://github.com/Mintplex-Labs/anything-llm) via the [Awesome LLM](https://github.com/Hannibal046/Awesome-LLM) list. It's been really easy to use: I ran
```sh
export STORAGE_LOCATION=$HOME/anythingllm && \
mkdir -p $STORAGE_LOCATION && \
touch "$STORAGE_LOCATION/.env" && \
docker run -d -p 3001:3001 \
--network anythingllm \
--network-alias anythingllm \
-v ${STORAGE_LOCATION}:/app/server/storage \
-v ${STORAGE_LOCATION}/.env:/app/server/.env \
-e STORAGE_DIR="/app/server/storage" \
mintplexlabs/anythingllm
```
and it was available.

For the setup, I used `http://ollama:11434` as the URL. Because of the `--network-alias` on the `docker run` command, this points at the Ollama container.